<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    function tentukan_nilai($number){
        if ($number >= 85 && $number < 100){
            echo $number . " => Sangat Baik (A)<br>";
        }else if ($number >= 75 && $number < 85){
            echo $number . " => Baik (B)<br>";
        }else if ($number >= 60 && $number < 75){
            echo $number . " => Cukup (C)<br>";
        }else{
            echo $number . " => Kurang (D)<br>";
        }
    }

    //TEST CASES
    tentukan_nilai(98); //Sangat Baik
    tentukan_nilai(76); //Baik
    tentukan_nilai(67); //Cukup
    tentukan_nilai(43); //Kurang
    ?>
</body>
</html>