<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
function tukar_besar_kecil($string){
    for($a = 0 ; $a < strlen($string) ; $a++){
        if($string[$a] == strtoupper($string[$a])){
            echo strtolower($string[$a]);
        }else{
            echo strtoupper($string[$a]);
        }
    }
    echo "<br>";
}

// TEST CASES
tukar_besar_kecil('Hello World'); // "hELLO wORLD"
tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
</body>
</html>